package filter

import "regexp"

type IFilter interface {
	Match(url string) bool
}

type RegexFilter struct {
	regex string
}

func NewRegexFilter(regex string) *RegexFilter {
	return &RegexFilter{regex: regex}
}

func (this *RegexFilter) Match(url string) bool {
	match, _ := regexp.MatchString(this.regex, url)
	return match
}
